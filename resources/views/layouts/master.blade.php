<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Insurance management - @yield('title')</title>

    </head>
    <body >
        <div class="container">
            @yield('main_content')
        </div>
    </body>
</html>